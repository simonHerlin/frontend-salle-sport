// import state from './general/state'
// import * as getters from './general/getters'
// import * as mutations from './general/mutations'
// import * as actions from './general/actions'

// export default {
//   namespaced: true,
//   state,
//   getters,
//   mutations,
//   actions
// }

import Vue from 'vue'
import Vuex from 'vuex'

import general from './general'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // then we reference it
      general,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: false
  })

    /*
    if we want some HMR magic for it, we handle
    the hot update like below. Notice we guard this
    code with "process.env.DEV" -- so this doesn't
    get into our production build (and it shouldn't).
  */

 if (process.env.DEV && module.hot) {
  module.hot.accept(['./general'], () => {
    const newGeneral = require('./general').default
    Store.hotUpdate({ modules: { general: newGeneral } })
  })
}

return Store
}
