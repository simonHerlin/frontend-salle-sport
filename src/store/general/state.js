export default function () {
  return {
    url: "http://127.0.0.1:8000/", // dev
    //url: '',prod
    status: '',
    token: localStorage.getItem('user-token') || '',
    config: {},
    bgColors: [
      'linear-gradient( 135deg, #FFFFCC 10%, #FFD700 100%)',
      'linear-gradient( 135deg, #ABDCFF 10%, #0396FF 100%)',
      'linear-gradient( 135deg, #2AFADF 10%, #4C83FF 100%)',
      'linear-gradient( 135deg, #FFD3A5 10%, #FD6585 100%)',
      'linear-gradient( 135deg, #EE9AE5 10%, #5961F9 100%)'
    ],
    username: localStorage.getItem('user-username') || '',
    //idClient: localStorage.getItem('user-token') || 0,
    clientInformation : localStorage.getItem('user-info') || {
      id: 0,
      first_name : '',
      last_name: '',
      date_of_birth: '',
      email: '',
      phone: '',
      code : '',
      permission: 0,
      city: '',
    },
    allMuscleSize: [],
    goal: []
  }
}
