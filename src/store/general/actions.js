import axios from 'axios'

export function login ({commit}, credential) {
    commit('AUTH_REQUEST')
    return new Promise ((resolve, reject) => {
        axios
            .post("http://127.0.0.1:8000/rest_auth/login/", credential)
            .then(({data}) => {
                localStorage.setItem('user-token', data.key)
                commit('AUTH_SUCCESS', data.key)
                commit('USERNAME', credential.username)
                resolve(data.key)
            })
            .catch(err => {
                localStorage.removeItem('user-token')
                commit('AUTH_ERROR')
                reject(err)
            })
    })
}

export function logout ({commit}) {
    return new Promise((resolve, reject) => {
        commit('AUTH_LOGOUT')
        localStorage.removeItem("user-token");
    })
}

export function initialisation(state) {
    state.commit('INITIALISATION_CLIENT')
}

export function updateInformation(state, newInformation) {
    state.commit('UPDATE', newInformation)
}