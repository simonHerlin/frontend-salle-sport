

export function lastMuscleSizeClient (state) {
  //state.allMuscleSize[state.allMuscleSize.length - 1].keys(obj).map(function (key) { return obj[key]; })
  //return state.allMuscleSize[state.allMuscleSize.length - 1]
  
  if (state.allMuscleSize.length > 0) {

    let muscle = state.allMuscleSize[state.allMuscleSize.length - 1]
    let goal = state.goal[state.goal.length - 1]

    let arrayMuscle = []
    let arrayGoal = []
    

    if (muscle !== undefined) {
      arrayMuscle = Object.values(muscle)
      arrayMuscle.splice(-3, 1)
      arrayMuscle.splice(-3, 1)
    }
    if (goal !== undefined) {
      arrayGoal = Object.values(goal)
      arrayGoal.splice(-3, 1)
      arrayGoal.splice(-3, 1)
    }

    let serie = [
      {
        name: "Taille muscle",
        data: arrayMuscle
      },
      {
        name: "Objectif",
        data: arrayGoal
      }
    ]
    return serie
  }
}

// export function bgColor (state, rang) {
//   return state.bgColors[rang]
// }
