import axios from 'axios'


export function AUTH_REQUEST(state) {
    state.status = 'loading'
}

export function AUTH_SUCCESS(state, token) {
    state.status = 'success'
    state.token = token
    state.conf = {
            headers: {
              'Authorization': 'Token ' + token
            }
        }
}

export function AUTH_ERROR(state){
    state.status = 'error'
}


export function AUTH_LOGOUT({commit, dispatch}) {
    state.status = ''
    state.token = ''
    state.conf = {}
}

export function USERNAME (state, username) {
    localStorage.setItem('user-username', username)
    state.username = username
}

export function INITIALISATION_CLIENT (state) {
    axios
        .get(state.url + "api/client/?username=" + state.username, state.conf)
        .then((response) => {
            localStorage.setItem('user-info', response.data[0])
            state.clientInformation = response.data[0]
            axios.
                get(state.url + 'api/muscle_size/?client=' + state.clientInformation.id + "&goal=false", state.conf)
                .then((response) => {
                    state.allMuscleSize = response.data
                })
            axios.
                get(state.url + 'api/muscle_size/?client=' + state.clientInformation.id + "&goal=true", state.conf)
                .then((response) => {
                    state.goal = response.data
                })
            // axios.
            //     get(state.url + 'api/last_muscle/?client=' + state.clientInformation.id + "&goal=false", state.conf)
            //     .then((response) => {
            //         //state.lastMuscle = response.data
            //         console.log(response.data)
            //     })
            // axios
            //     .get(state.url + 'api/last_muscle/?client=' + state.clientInformation.id + "&goal=true", state.conf)
            //     .then((response) => {
            //         //state.goal = response.data
            //         console.log(response.data)
            //     })
        })
}


export function UPDATE (state, newInformation) {
    state.clientInformation = newInformation
}
