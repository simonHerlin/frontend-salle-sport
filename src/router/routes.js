
const routes = [
  {
    path: '',
    component: () => import('pages/Login.vue')
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      //{path: '', component: () => import('pages/Connexion.vue')}
      //{ path: '', component: () => import('pages/Index.vue') }
      {path: 'General', component: () => import('pages/General.vue')},
      {path: 'TailleMuscle', component: () => import('pages/TailleMuscle.vue')},
      {path: 'Performance', component: () => import('pages/Performance.vue')},
      {path: 'Nutrition', component: () => import('pages/Nutrition.vue')},
      {path: 'Objectif', component: () => import('pages/Objectif.vue')},
      //{path: 'connexion', component: () => import('pages/Connexion.vue')}
    ],
    //path: 'connexion', component: () => import('pages/Connexion.vue')
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}


export default routes